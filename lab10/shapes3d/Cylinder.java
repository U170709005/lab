package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    private double height;

    public Cylinder() {
        super(1);
        this.height = 1;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    @Override
    public double area() {
        return 2 * super.area() + height * 2 * Math.PI * radius;
    }

    public double volume() {
        return super.area() * height;
    }
}
